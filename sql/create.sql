create table Subscriptions
( client_id  int unsigned not null
, vehicle_id int unsigned not null
, primary key (client_id, vehicle_id)
);
