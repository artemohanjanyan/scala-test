package test.task

import java.net.SocketAddress
import java.sql.{DriverManager, PreparedStatement, Statement}
import java.util.concurrent.{ConcurrentHashMap, ConcurrentMap}
import collection.JavaConverters._

object Server extends App {
  ServerBase.mainTemplate[Server]("port db_address db_user db_password") {
    val port = args(0).toInt
    val server = new Server()
    server.start(port, new ServerMysqlDatabase(args(1), args(2), args(3)))
    server
  } { server =>
    while (!Thread.interrupted()) {
      server.synchronized {
        server.wait()
      }
    }
  }

  class ServerMysqlDatabase(address: String, user: String, password: String) extends ServerDatabase {
    private val connection = DriverManager.getConnection(s"jdbc:mysql://$address", user, password)

    override def backup(subscriptions: ConcurrentMap[VehicleId, java.util.Set[ClientId]]): Unit = {
      var removeStatement: Statement = null
      var backupStatement: PreparedStatement = null
      try {
        connection.setAutoCommit(false)

        removeStatement = connection.createStatement()
        removeStatement.execute("delete from Subscriptions")

        backupStatement = connection.prepareStatement("insert into Subscriptions (client_id, vehicle_id) values (?, ?)")
        for {
          (vehicleId, clients) <- subscriptions.asScala
          clientId <- clients.asScala
        } {
          backupStatement.setInt(1, clientId.id)
          backupStatement.setInt(2, vehicleId.id)
          backupStatement.addBatch()
        }
        backupStatement.executeBatch()

        connection.commit()
      } finally {
        if (backupStatement != null) {
          backupStatement.close()
        }
        if (removeStatement != null) {
          removeStatement.close()
        }
        connection.setAutoCommit(true)
      }
    }

    override def getBackup: ConcurrentMap[VehicleId, java.util.Set[ClientId]] = {
      var statement: Statement = null
      try {
        statement = connection.createStatement()
        val resultSet = statement.executeQuery("select * from Subscriptions")
        val result = new ConcurrentHashMap[VehicleId, java.util.Set[ClientId]]()
        while (resultSet.next()) {
          val clientId = ClientId(resultSet.getInt(1))
          val vehicleId = VehicleId(resultSet.getInt(2))
          result.computeIfAbsent(vehicleId, _ => ConcurrentHashMap.newKeySet[ClientId]())
          result.get(vehicleId).add(clientId)
        }
        result
      } finally {
        if (statement != null) {
          statement.close()
        }
      }
    }

    override def close(): Unit = {
      connection.close()
    }
  }
}

trait ServerDatabase extends AutoCloseable {
  def backup(subscriptions: ConcurrentMap[VehicleId, java.util.Set[ClientId]])
  def getBackup: ConcurrentMap[VehicleId, java.util.Set[ClientId]]
}

class Server extends ServerBase {
  private val vehicles = new ConcurrentHashMap[VehicleId, (SocketAddress, Position)]().asScala
  private val clients = new ConcurrentHashMap[ClientId, SocketAddress]().asScala
  private val subscriptions = new ConcurrentHashMap[VehicleId, java.util.Set[ClientId]]()
  private var database: ServerDatabase = _

  def start(port: Int, database: ServerDatabase): Unit = {
    if (database != null) {
      this.database = database
      subscriptions.putAll(database.getBackup)
      executor.execute(() => backupLoop())
    }

    super.start(port, () => serverLoop())
  }

  private def serverLoop(): Unit =
    ServerBase.receiveLoop[Msg](socket, log = true) { (packet, msg) =>
      msg match {
        case VehiclePositionMsg(id, position) =>
          vehicles.put(id, (packet.getSocketAddress, position))
          for {
            subscribers <- subscriptions.asScala.get(id)
            client <- subscribers.asScala
            clientAddress <- clients.get(client)
          } {
            packet.setSocketAddress(clientAddress)
            socket.send(packet)
          }
        case HeartbeatMsg(clientId) =>
          clients.put(clientId, packet.getSocketAddress)
        case SubscribeMsg(clientId, vehicleId) =>
          subscriptions.computeIfAbsent(vehicleId, _ => ConcurrentHashMap.newKeySet[ClientId]())
          subscriptions.get(vehicleId).add(clientId)
        case UnsubscribeMsg(clientId, vehicleId) =>
          subscriptions.computeIfAbsent(vehicleId, _ => ConcurrentHashMap.newKeySet[ClientId]())
          subscriptions.get(vehicleId).remove(clientId)
        case MoveMsg(vehicleId, _) =>
          for {
            (vehicleAddress, _) <- vehicles.get(vehicleId)
          } {
            packet.setSocketAddress(vehicleAddress)
            socket.send(packet)
          }
      }
    }

  private def backupLoop(): Unit = {
    while (!Thread.interrupted()) {
      Thread.sleep(10000)
      database.backup(subscriptions)
    }
  }

  override def close(): Unit = {
    if (database != null) {
      try {
        database.close()
      } catch {
        case _: Throwable =>
      }
    }
    super.close()
  }
}
