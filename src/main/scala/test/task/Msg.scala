package test.task

case class VehicleId(id: Int)
case class Position(x: Double, y: Double) {
  def +(that: Position): Position = Position(this.x + that.x, this.y + that.y)
  def -(that: Position): Position = Position(this.x - that.x, this.y - that.y)
  def *(k: Double): Position = Position(x * k, y * k)
  def /(k: Double): Position = Position(x / k, y / k)
  def length(): Double = Math.sqrt(x * x + y * y)
}
case class ClientId(id: Int)

sealed abstract class Msg extends Serializable

case class VehiclePositionMsg(id: VehicleId, position: Position) extends Msg

case class HeartbeatMsg(clientId: ClientId) extends Msg
case class SubscribeMsg(clientId: ClientId, vehicleId: VehicleId) extends Msg
case class UnsubscribeMsg(clientId: ClientId, vehicleId: VehicleId) extends Msg
case class MoveMsg(vehicleId: VehicleId, newPosition: Position) extends Msg