package test.task

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, ObjectInputStream, ObjectOutputStream}
import java.net.{DatagramPacket, DatagramSocket, SocketException}
import java.sql.SQLException
import java.util.concurrent.{ExecutorService, Executors}

object ServerBase {
  def fromPacket[T <: Msg](datagramPacket: DatagramPacket): T =
    new ObjectInputStream(new ByteArrayInputStream(
      datagramPacket.getData,
      datagramPacket.getOffset,
      datagramPacket.getLength
    )).readObject().asInstanceOf[T]

  def toPacket[T <: Msg](t: T): DatagramPacket = {
    val outputStream = new ByteArrayOutputStream()
    new ObjectOutputStream(outputStream).writeObject(t)
    val array = outputStream.toByteArray
    new DatagramPacket(array, array.length)
  }

  def receiveLoop[T <: Msg](socket: DatagramSocket, log: Boolean)
                           (action: (DatagramPacket, T) => Unit): Unit = {
    val requestBuffer = new Array[Byte](1024)
    val request = new DatagramPacket(requestBuffer, requestBuffer.length)

    while (!Thread.interrupted()) {
      socket.receive(request)
      val msg = fromPacket[T](request)
      if (log) {
        println(s"Received $msg from ${request.getSocketAddress}")
      }

      action(request, msg)
    }
  }

  def mainTemplate[T >: Null <: ServerBase](arguments: String)
                                           (createServer: => T)
                                           (thenDo: T => Unit): Unit = {
    var server: T = null
    try {
      server = createServer
      thenDo(server)
    } catch {
      case e: Throwable =>
        Console.err.println(e match {
          case _: ArrayIndexOutOfBoundsException => "Not enough arguments"
          case _: NumberFormatException => "Incorrect number"
          case _: SocketException => "Couldn't open UDP socket"
          case _: InterruptedException => "Interrupted"
          case _: SQLException => s"SQL error:\n${e.getMessage}"
          case _ =>
            e.printStackTrace()
            "Unexpected error"
        })
        Console.err.println(s"Arguments: $arguments")
    } finally {
      if (server != null) {
        server.close()
      }
    }
  }
}

abstract class ServerBase extends AutoCloseable {
  protected var socket: DatagramSocket = _
  protected val executor: ExecutorService = Executors.newCachedThreadPool()

  //noinspection ScalaUnnecessaryParentheses
  protected def start(port: Int, actions: (() => Unit)*): Unit = {
    socket = new DatagramSocket(port)
    for (action <- actions) {
      executor.execute(() => action())
    }
  }

  override def close(): Unit = {
    var closeException: Throwable = null
    if (socket != null) {
      try {
        socket.close()
      } catch {
        case t: Throwable => closeException = t
      }
    }
    if (executor != null) {
      executor.shutdownNow()
    }
    if (closeException != null) {
      throw closeException
    }
  }
}
