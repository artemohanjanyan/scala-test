package test.task

import java.net._
import java.util.concurrent.atomic.AtomicReference

object Vehicle extends App {
  ServerBase.mainTemplate[Vehicle]("id port server_address server_port speed n") {
    val id = VehicleId(args(0).toInt)
    val port = args(1).toInt
    val serverAddress = new InetSocketAddress(args(2), args(3).toInt)
    val vehicle = new Vehicle(id, args(4).toDouble, args(5).toDouble)
    vehicle.start(port, serverAddress)
    vehicle
  } { vehicle =>
    while (!Thread.interrupted()) {
      vehicle.synchronized {
        vehicle.wait()
      }
    }
  }
}

class Vehicle(id: VehicleId, speed: Double, n: Double) extends ServerBase {
  private val position = new AtomicReference[Position](Position(0, 0))
  private val destination = new AtomicReference[Position](Position(0, 0))
  private var serverAddress: SocketAddress = _

  def start(port: Int, serverAddress: SocketAddress): Unit = {
    super.start(port, () => reportLoop(), () => receiveLoop(), () => moveLoop())
    this.serverAddress = serverAddress
  }

  private def report(): Unit = {
    val packet = ServerBase.toPacket(VehiclePositionMsg(id, position.get()))
    packet.setSocketAddress(serverAddress)
    socket.send(packet)
  }

  private def reportLoop(): Unit = {
    report()
    while (!Thread.interrupted()) {
      Thread.sleep((n * 1000).toLong)
      report()
    }
  }

  private def moveLoop(): Unit = {
    val sleep = 10
    while (!Thread.interrupted()) {
      Thread.sleep(sleep)

      val pos = position.get()
      val dst = destination.get()
      val direction = dst - pos
      val distance = direction.length()
      if (distance < speed) {
        position.set(dst)
      } else {
        position.set(pos + direction / distance * speed * sleep / 1000)
      }
    }
  }

  private def receiveLoop(): Unit =
    ServerBase.receiveLoop[MoveMsg](socket, log = true) { (_, msg) =>
      this.synchronized {
        destination.set(msg.newPosition)
      }
    }
}
