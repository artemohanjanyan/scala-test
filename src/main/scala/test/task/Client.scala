package test.task

import java.io.IOException
import java.net.{InetSocketAddress, SocketAddress}

import scala.collection.mutable.ListBuffer

object Client extends App {
  ServerBase.mainTemplate[Client]("id port server_address server_port heartbeat_period") {
    val id = ClientId(args(0).toInt)
    val port = args(1).toInt
    val serverAddress = new InetSocketAddress(args(2), args(3).toInt)
    val client = new Client(id, args(4).toDouble)
    client.start(port, serverAddress)
    client
  } { client =>
    print(
      """Available commands:
        |subscribe vehicle_id
        |unsubscribe vehicle_id
        |move vehicle_id x y
        |notifications
        |""".stripMargin)
    while (true) {
      print("> ")
      try {
        Console.in.readLine().split(" ") match {
          case Array("subscribe", idStr) =>
            client.subscribe(VehicleId(idStr.toInt))
          case Array("unsubscribe", idStr) =>
            client.unsubscribe(VehicleId(idStr.toInt))
          case Array("move", idStr, xStr, yStr) =>
            client.moveMsg(
              VehicleId(idStr.toInt),
              Position(xStr.toDouble, yStr.toDouble)
            )
          case Array("notifications") =>
            client.pollNotifications().foreach(println)
          case _ =>
            println("Incorrect command")
        }
      } catch {
        case _: NumberFormatException => println("Incorrect number")
        case _: IOException | _: NullPointerException =>
          println("Bye")
          client.close()
      }
    }
  }
}

class Client(id: ClientId, heartbeatPeriod: Double) extends ServerBase {
  private var serverAddress: SocketAddress = _
  private val notifications = ListBuffer[String]()

  def start(port: Int, serverAddress: SocketAddress): Unit = {
    super.start(port, () => receiveLoop(), () => heartbeatLoop())
    this.serverAddress = serverAddress
  }

  private def sendMsg[T <: Msg](msg: T): Unit = {
    val packet = ServerBase.toPacket(msg)
    packet.setSocketAddress(serverAddress)
    socket.send(packet)
  }

  def subscribe(vehicleId: VehicleId): Unit =
    sendMsg(SubscribeMsg(id, vehicleId))

  def unsubscribe(vehicleId: VehicleId): Unit =
    sendMsg(UnsubscribeMsg(id, vehicleId))

  def moveMsg(vehicleId: VehicleId, newPosition: Position): Unit =
    sendMsg(MoveMsg(vehicleId, newPosition))

  def pollNotifications(): List[String] =
    notifications.synchronized {
      val ans = notifications.toList
      notifications.clear()
      ans
    }

  private def heartbeatLoop(): Unit = {
    val heartbeatPacket = ServerBase.toPacket(HeartbeatMsg(id))
    heartbeatPacket.setSocketAddress(serverAddress)
    while (!Thread.interrupted()) {
      Thread.sleep((heartbeatPeriod * 1000).toLong)
      socket.send(heartbeatPacket)
    }
  }

  private def receiveLoop(): Unit =
    ServerBase.receiveLoop[VehiclePositionMsg](socket, log = false) { (_, msg) =>
      notifications.synchronized {
        notifications += s"${msg.id} moved to ${msg.position}"
      }
    }
}
